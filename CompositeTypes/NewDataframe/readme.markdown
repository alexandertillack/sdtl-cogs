Creates a new empty dataframe.  Numbers of rows or columns may be specified.  All values are assumed to be missing.
